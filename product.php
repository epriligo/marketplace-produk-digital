<?php
include ("functions.php");
$produk = query("SELECT * FROM produk ");
// $produk = mysqli_fetch_assoc($rsltproduk);

//search query 
if(isset($_POST["search"])){
    $produk = search($_POST["keyword"]);
}
?>




<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/product_style.css">
    <script src="produk.js"></script>
    <title>Product</title>
</head>
<body>
    <!--menu  di header-->
    <nav>
        <div class="icon">Cryfun-Pedia</div>
        <form action="" method="post">
        <div class="search_box">
            <input type="text" name="keyword" placeholder="search product">
        <button type="submit" name="search"><span class="fa fa-search"></span></button>
            
        </div>
        </form>
        <ol>
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About </a></li>
            <li><a href="login.php" class="tbl-biru">Login </a></li>

        </ol>
     </nav>

     <!--Our Product-->
     <section class="product-card">
        <div class="flex" >
            <?php foreach($produk as $row) : ?>
             <div class="card">
                 <div class="img-card">
                     <img src="img/<?php echo $row["gambar"]; ?>" class="img"/>
                 </div>
                     <div class="content-text">
                     <center><h3><?php echo $row["nama"]; ?></h3></center> 
                     <center><h3 class="harga"><?php echo $row["harga"]; ?></h3></center>
                     <br>
                     <div class="btn-block">
                         <a href="order.php?id=<?=$row["id"];?>" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
            <?php endforeach; ?>
             <!-- <div class="card">
                 <div class="img-card">
                     <img src="img/appleimg.png" class="img"/>
                 </div>
                     <div class="content-text">
                     <h3><center>Apple Music</center></h3> 
                     <h3 class="harga"><center>Rp. 30.000</center></h3>
                     <br>
                     <div class="btn-block">
                         <a href="order.html" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
 
             <div class="card">
                 <div class="img-card">
                     <img src="img/amazonimg.png" class="img"/>
                 </div>
                     <div class="content-text">
                     <h3><center>Amazon Premium</center></h3> 
                     <h3 class="harga"><center>Rp. 65.000</center></h3>
                     <br>
                     <div class="btn-block">
                         <a href="order.html" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
 
             <div class="card">
                 <div class="img-card">
                     <img src="img/canvaimg.png" class="img"/>
                 </div>
                     <div class="content-text">
                     <h3><center>Canva</center></h3> 
                     <h3 class="harga"><center>Rp. 30.000</center></h3>
                     <br>
                     <div class="btn-block">
                         <a href="order.html" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
 
             <div class="card">
                 <div class="img-card">
                     <img src="img/produk1.jpg" class="img"/>
                 </div>
                     <div class="content-text">
                     <h3>E-book</h3> 
                     <h3 class="harga">Rp. 50.000</h3>
                     <br>
                     <div class="btn-block">
                         <a href="order.html" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
 
         </div>
    <br>     
         <div class="flex" >
            <div class="card">
                <div class="img-card">
                    <img src="img/teleimg.jpg" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Telegram Premium</center></h3> 
                    <center><h3 class="harga">Rp. 75.000</h3></center>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <div class="img-card">
                    <img src="img/netflix (1).png" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Netflix </center></h3> 
                    <h3 class="harga"><center>Rp. 60.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="img-card">
                    <img src="img/spimg.png" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Spotify</center></h3> 
                    <h3 class="harga"><center>Rp. 35.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="img-card">
                    <img src="img/disney (1).png" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>disney</center></h3> 
                    <h3 class="harga"><center>Rp. 50.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="img-card">
                    <img src="img/vpnnordimg.png" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Vpn Nord</center></h3> 
                    <h3 class="harga"><center>Rp. 70.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div> -->

        </div>
     </section>
    
</body>
</html>