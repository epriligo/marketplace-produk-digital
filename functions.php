<?php
//conection to database
$conn = mysqli_connect("localhost","root","","marketplace");

function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)){
        $rows[]= $row;
    }
    return $rows;
}

function registrasi($data){
    global $conn;

    $username = strtolower(stripslashes($data["username"]));
    $email    =($data["email"]);
    $password = mysqli_real_escape_string($conn,$data["password"]);
    $cpassword = mysqli_real_escape_string($conn,$data["cpassword"]);

    if ( $password !== $cpassword){
        echo "<script>
            alert('password do not match');
        </script>";

    return false;
    }

$result = mysqli_query($conn,"SELECT username FROM user 
    WHERE username = '$username'");

if (mysqli_fetch_assoc($result)) {
    echo " <script> 
          alert('username already registered')
          </script>    
    ";
    return false;
}

//encriptions
$password = password_hash($password, PASSWORD_DEFAULT);

//add to database
mysqli_query($conn,"INSERT INTO user VALUES('','$username','$email','$password','user')");
    
    return mysqli_affected_rows($conn);
}

//for insert data
function tambah($data){
    global $conn;
    $nama = htmlspecialchars($data["nama"]);
    $harga = htmlspecialchars($data["harga"]);
    $jumlah = htmlspecialchars($data["jumlah"]);
    $deskripsi =htmlspecialchars($data["deskripsi"]);
   
    //upload gambar
    $gambar =upload();
    if(!$gambar){
        return false;
    }


    $query = "INSERT INTO produk
    VALUES 
    ('','$nama','$harga','$jumlah','$deskripsi','$gambar')
    ";
    mysqli_query($conn,"$query");
    
    return mysqli_affected_rows($conn);


}
//for search
function search($keyword){
    $query = "SELECT * FROM produk 
                WHERE 
                nama LIKE '%$keyword%'
            ";
    return query($query);
}
//for upload 
function upload (){
    $namaFile = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpName = $_FILES['gambar']['tmp_name'];

    //cek error
    if($error === 4){
        echo "<script>
                alert('you not chosee image')
              </script>";
        return false;
    }
    //cek file
    $ekstesiGambarValid =['jpg','jpeg','png'];
    $ekstesiGambar = explode('.',$namaFile);
    $ekstesiGambar = strtolower(end($ekstesiGambar));
    if(!in_array($ekstesiGambar,$ekstesiGambarValid)){
        echo "<script>
        alert('its not a image')
      </script>";
      return false;
    }
    //move store file
    move_uploaded_file($tmpName,'img/' . $namaFile);

    return $namaFile;
}

//for delete data
function delete($id){
    global $conn;
    mysqli_query($conn,"DELETE FROM produk WHERE id=$id");
    return mysqli_affected_rows($conn);
}


?>