<?php
session_start();

if(!isset($_SESSION["login"])){
    header("location: login.php");
    exit;
}
if ($_SESSION["level"] !== "admin"){
    header("location: login.php");
    exit;
}
require 'functions.php';
$product=query("SELECT * FROM produk");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <script src="fontawesome/all.js"></script>
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/table_style.css">
</head>
<body>
    <main>
        <input type="checkbox" id="check">
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fa fa-arrow-right" id="open"></i>
        </label>
        <div class="sidebar">
            <div class="top">
                Dashboard
            </div>  
            <ul>
                <li><a class="#" href=""><i class="fa fa-home"></i> Profile</a></li>
                <li><a class="#" href="addproduct.php"><i class="fa fa-shopping-basket"></i> Input Product</a></li>
                <li><a class="#" href="#"><i class="fa fa-shopping-bag"></i> Stock Products</a></li>
                <li><a class="#" href="logout.php"><i class="fa fa-user-circle"></i> LogOut</a></li>
            </ul>
        </div>
    </main>
    </div>
    <div>
        <center>
        <table border="1" align="center">
            <tr>
                <th width="50">No</th>
                <th width="150">Name</th>
                <th width="100">Price</th>
                <th width="70">Amount</th>
                <th width="250">Description</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            <?php $i = 1; ?>
            <?php foreach($product as $row) : ?>
            <tr>
                <td><?= $i ?></td>
                <td><?=$row['nama']; ?></td>
                <td><?=$row['harga']; ?></td>
                <td><?=$row['jumlah'];?></td>
                <td><?=$row['deskripsi']; ?></td>
                <td><img src="img/<?=$row['gambar']; ?>" width="75"></td>
                <td>
                    <a href="ubah.php?id=<?=$row["id"];?>"><button type="submit" class="btn-update">Edit</button></a>
                    <a href="delete.php?id=<?=$row["id"];?>"><button type="submit" class="btn-delete">Delete</button></a>
                </td>
            </tr>
            <?php $i++; ?>
            <?php endforeach;?>
        </table>
        </center>
    </div>
</body>
</html>