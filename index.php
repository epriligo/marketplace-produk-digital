<?php
include ("functions.php");
$produk = query("SELECT * FROM produk LIMIT 0,5");
// $produk = mysqli_fetch_assoc($rsltproduk);

//search query 
// if(isset($_POST["search"])){
//     $produk = search($_POST["keyword"]);
// }
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cryfun-Pedia</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--<script src="https://kit.fonrawesome.com/67c66657c7.js"></script>-->
</head>
<body>
    <!--menu  di header-->
    <nav>
        <div class="icon">Cryfun-Pedia</div>
        <div class="search_box">
            <input type="search" placeholder="search product">
            <span class="fa fa-search"></span>
        </div>
        <ol>
            <li><a href="product.php">Product</a></li>
            <li><a href="about.html">About </a></li>
            <li><a href="login.php" class="tbl-biru">Login </a></li>

        </ol>
    </nav>
    <!--banner-->
    <div class="wrapper">
    <section id="home">
        <img src="img/gambar1.jpg" width="45%"/>
        <div class="kolom">
            <p class="deskripsi">"Your Trust is Our Pride"</p>
            <h2 class="destxt">Marketplace Safe and trusted virtual product. Prioritizing security and convenience in online transactions</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, urna consequat felis vehicula class ultricies<br>
                 mollis dictumst, aenean non a in donec nulla. Phasellus ante pellentesque erat cum risus consequat<br> 
                 imperdiet aliquam, integer placerat et turpis mi eros nec lobortis taciti, vehicula nisl </p>
            <p><a href="" class="tbl-pink">View Offers</a></p>
        </div>
    </section>
    </div>
            <div class="text-product"><h2>Available Products</h2></div> 
    <!-- Product-->
    <section class="product-card">
    <div class="flex" >
            <?php foreach($produk as $row) : ?>
             <div class="card">
                 <div class="img-card">
                     <img src="img/<?php echo $row["gambar"]; ?>" class="img"/>
                 </div>
                     <div class="content-text">
                     <center><h3><?php echo $row["nama"]; ?></h3></center> 
                     <center><h3 class="harga"><?php echo $row["harga"]; ?></h3></center>
                     <br>
                     <div class="btn-block">
                         <a href="order.php?id=<?=$row["id"];?>" class="btn-buy">BUY</a>
                     </div>
                 </div>
             </div>
            <?php endforeach; ?>
            
            <!-- <div class="card">
                <div class="img-card">
                    <img src="img/officeimgr.jpg" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Licensee microsoft office</center></h3> 
                    <h3 class="harga"><center>Rp. 90.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div> -->

            <!-- <div class="card">
                <div class="img-card">
                    <img src="img/teleimg.jpg" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Telegram Premium</center></h3> 
                    <h3 class="harga"><center>Rp. 35.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="img-card">
                    <img src="img/amazonimg.png" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center>Amazon prime</center></h3> 
                    <h3 class="harga"><center>Rp. 35.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="img-card">
                    <img src="img/malwareim.jpg" class="img"/>
                </div>
                    <div class="content-text">
                    <h3><center> License Malwarebyte</center></h3> 
                    <h3 class="harga"><center>Rp. 65.000</center></h3>
                    <br>
                    <div class="btn-block">
                        <a href="order.html" class="btn-buy">BUY</a>
                    </div>
                </div>
            </div> -->

        </div>
    </section>
    
    <footer>
        <div class="main-content">
          <div class="left box">
            <h2>About us</h2>
            <div class="content">
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat corporis aspernatur ipsum corrupti placeat temporibus asperiores officia impedit, blanditiis et..</p>
              <div class="social">
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
              </div>
            </div>
          </div>
          <div class="center box">
            <h2>Quick Links</h2>
            <div class="content">
            <ul class="box">
              
              <li><a href="">About</a></li>
              <li><a href="#">FaQ</a></li>
              <li><a href="#">Help</a></li>
              <li><a href="#">Term & Conditions</a></li>
              <li><a href="">Privacy</a></li>
            </ul>
            </div>
          </div>
          <div class="right box">
            <h2>Contact us</h2>
            <div class="content">
              <form  method="post" action="kirim.php">
                <div class="email">
                  <div class="text">Email *</div>
                  <input type="email" name="email" required>
                </div>
                <div class="msg">
                  <div class="text">Message *</div>
                  <textarea rows="2" cols="25" name="message"required></textarea>
                </div>
                <div class="btn">
                  <button type="submit" name="sentemail">Send</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="bottom">
          <center>
            <span class="credit">Created By <a href="#">Epriligo</a> | </span>
            <i class="fa fa-copyright" aria-hidden="true"></i><span> 2022 All rights reserved.</span>
          </center>
        </div>
      </footer>

</body>
</html>