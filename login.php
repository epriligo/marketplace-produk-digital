<?php
session_start();
require 'functions.php';

if(isset($_POST["submit"])){
    $username = $_POST["username"];
    $password = $_POST["password"];

$result =   mysqli_query($conn, "SELECT * FROM user WHERE
 username = '$username'");

if (mysqli_num_rows($result) === 1){

    $row = mysqli_fetch_assoc($result);
        if(password_verify($password, $row["password"])) {

            //set sesion
            $_SESSION["login"]=true;
            $_SESSION["id"]=$row["id"];
            $_SESSION["level"]=$row["level"];
            
        if($_SESSION["level"] == "admin"){
            header("Location: dashboard.php");
            // exit;
        }else{
            header("location: dashboarduser.php");
        }
        }
    }
    $error = true;
}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login_style.css">
    <title>Login</title>
</head>
<body>
    <div class="form-container">
        <form action="" method="post">
            <h3>Login Now</h3>
            <input type="text" name="username" required placeholder="enter your username">
            <input type="password" name="password" required placeholder="enter your password">
            <input type="submit" name="submit" value="login now" class="form-btn">
            <p>don't have an account? <a href="register.php">register now</a></p>   
        </form>
    </div>
</body>
</html>